/**
 @file bmode/L12_3v_mlbf.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-07-17

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <cuda.h>
#include <stdio.h>
#include <iostream>
#include <typeinfo>
#include "Bmode.cuh"
#include "ChannelSum.cuh"
#include "FocusSynAp.cuh"
#include "NeuralNetwork.cuh"
#include "gpuBF.cuh"
#include "vsxBF_utils.cuh"
extern "C" {
#include <mex.h>
}

// Function prototypes; these functions should be defined
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void reset();                  // Clean up function
void atExit();                 // Function executed on MATLAB clear/exit
void *executeThread(void *p);  // Function to be used with POSIX threads
int main();                    // Dummy function; not used

// Flag to indicate if the MEX function has been initialized
static bool initialized = false;

// Variables for CUDA stream processing
static cudaStream_t stream;  // CUDA stream for asynchronous processing

// Declare the desired data processing stages
typedef short VSXType;   // Raw data from Verasonics is int16
typedef float2 FocType;  // Keep intermediate data as float2
typedef float ImgType;   // Output images will be float
static rtbf::VSXDataFormatter<VSXType, FocType> D;  // Format raw data
static rtbf::FocusSynAp<FocType, FocType> F;        // Focus raw data
static rtbf::ChannelSum<FocType, FocType> S1;       // Make subapertures
static rtbf::ChannelSum<FocType, FocType> S2;       // Sum channels
static rtbf::Bmode<FocType, ImgType> B;             // Detect envelope
static rtbf::NeuralNetwork<FocType, ImgType> N;     // Neural network

// Gateway function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // Tell MATLAB which function to run when calling clear mex, clear all, or
  // upon exiting
  mexAtExit(atExit);

  // If the input is a structure and not data, then call the reset function
  if (nrhs > 0 && mxIsStruct(prhs[0])) {
    reset();
  }

  // On first run, initialize
  if (!initialized) {
    // Get first available GPU
    int gpuID;
    rtbf::getFirstAvailableGPU(&gpuID);

    // Get reconstruction parameters stored in MATLAB structure R
    const mxArray *R = prhs[0];

    // Get required scalars
    int nsamps, npulses, nxmits, nelems, nchans, nrows, ncols;
    float outspwl;
    rtbf::getScalar(R, "nsamps", &nsamps);    // Number of temporal samples
    rtbf::getScalar(R, "npulses", &npulses);  // Number of repetitions
    rtbf::getScalar(R, "nxmits", &nxmits);    // Number of unique acquisitions
    rtbf::getScalar(R, "nelems", &nelems);    // Number of total elements
    rtbf::getScalar(R, "nchans", &nchans);    // Number of channels per acq.
    rtbf::getScalar(R, "nrows", &nrows);      // Number of output image rows
    rtbf::getScalar(R, "ncols", &ncols);      // Numer of output image columns
    rtbf::getScalar(R, "outspwl", &outspwl);  // Output samples per wavelength

    // Get required delay and apodization tables with error checking
    float *delTx, *apoTx, *delRx, *apoRx;
    rtbf::getDelayApodTables(R, "delTx", &delTx, nrows, ncols, nxmits);
    rtbf::getDelayApodTables(R, "apoTx", &apoTx, nrows, ncols, nxmits);
    rtbf::getDelayApodTables(R, "delRx", &delRx, nrows, ncols, nchans);
    rtbf::getDelayApodTables(R, "apoRx", &apoRx, nrows, ncols, nchans);

    // Get the per-transmit channel mapping
    int *chMap;
    rtbf::getChannelMapping(R, "chanmap", &chMap, nchans, nxmits, nelems);

    // Get the VSXSampleMode
    rtbf::VSXSampleMode vsxMode;
    float spcy;
    rtbf::getVSXSampleMode(R, "sampleMode", &vsxMode, &spcy);

    // Get the synthesis mode
    rtbf::SynthesisMode synMode;
    rtbf::getSynthesisMode(R, "synthesisMode", &synMode);

    // Initialize cudaStream
    CCE(cudaSetDevice(gpuID));
    CCE(cudaStreamCreate(&stream));

    // Initialize GPU objects
    int vv = 2;  // verbosity
    D.initialize(nsamps, nxmits, nelems, nchans, vsxMode, npulses, chMap, gpuID,
                 stream, vv);
    F.initialize(&D, nrows, ncols, outspwl, delTx, delRx, apoTx, apoRx, spcy,
                 synMode, stream, vv);
    S1.initialize(&F, 16, stream, vv);
    N.initialize(&S1, "network.uff", "input", "output", stream, vv);
    S2.initialize(&S1, 1, stream, vv);
    B.initialize(&S2, stream, vv);

    // Mark as initialized
    initialized = true;
  } else {  // If already initialized, real-time loop

    // Get reconstruction parameters stored in MATLAB structure R
    VSXType *h_raw = (VSXType *)mxGetData(prhs[0]);
    int pitchInSamps = mxGetM(prhs[0]);

    // Format the raw data using the transducer DataFormatter object
    D.formatRawVSXData(h_raw, pitchInSamps);
    F.focus();
    S1.sumChannels();
    N.execute();
    S2.sumChannels();
    B.getEnvelopeLogCompress();

    // Copy output to a MATLAB array
    if (nlhs > 0) rtbf::copyToMATLABArrayAsync(&B, &(plhs[0]), stream);
    if (nlhs > 1) rtbf::copyToMATLABArrayAsync(&N, &(plhs[1]), stream);
  }
}

// Reset function
void reset() {
  if (initialized) {
    B.reset();
    S2.reset();
    N.reset();
    S1.reset();
    F.reset();
    D.reset();
    CCE(cudaStreamDestroy(stream));
    initialized = false;
  }
}

// On MATLAB clear or exit, call reset()
void atExit() { reset(); }

// Dummy function
int main() { return 0; }