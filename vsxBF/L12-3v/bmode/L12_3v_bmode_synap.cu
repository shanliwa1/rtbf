/**
 @file bmode/L12_3v_bmode_synap.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-07-01

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <cuda.h>
#include <cstdio>
#include <iostream>
#include <typeinfo>
#include "Bmode.cuh"
#include "ChannelSum.cuh"
#include "FocusSynAp.cuh"
#include "HilbertTransform.cuh"
#include "gpuBF.cuh"
#include "vsxBF_utils.cuh"
extern "C" {
#include <mex.h>
}

// Function prototypes; these functions should be defined
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void reset();                  // Clean up function
void atExit();                 // Function executed on MATLAB clear/exit
void *executeThread(void *p);  // Function to be used with POSIX threads
int main();                    // Dummy function; not used

// Flag to indicate if the MEX function has been initialized
static bool initialized = false;

// Variables for CUDA stream processing
static cudaStream_t stream;  // CUDA stream for asynchronous processing

// Declare the desired data processing stages
typedef short VSXType;                     // Raw data from Verasonics is int16
typedef float2 FocType;                    // Keep intermediate data as float2
typedef float ImgType;                     // Output images will be float
static rtbf::VSXDataFormatter<FocType> D;  // Format raw data
static rtbf::FocusSynAp<FocType, FocType> F;  // Focus raw data
static rtbf::ChannelSum<FocType, FocType> S;  // Sum focused data
static rtbf::Bmode<FocType, ImgType> B;       // Detect envelope

// Variable to keep track of the sampling mode
static rtbf::VSXSampleMode vsxMode;

// Gateway function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // Tell MATLAB which function to run when calling clear mex, clear all, or
  // upon exiting
  mexAtExit(atExit);

  // If the input is a structure and not data, then call the reset function
  if (nrhs > 0 && mxIsStruct(prhs[0])) {
    reset();
  }

  // On first run, initialize
  if (!initialized) {
    // Get first available GPU
    int gpuID;
    rtbf::getFirstAvailableGPU(&gpuID);

    // Get reconstruction parameters stored in MATLAB structure R
    const mxArray *R = prhs[0];

    // Get required scalars
    int nsamps, npulses, nxmits, nelems, nchans, nrows, ncols;
    float outspwl;
    rtbf::getReqScalar(R, "nsamps", &nsamps);    // Number of temporal samples
    rtbf::getReqScalar(R, "npulses", &npulses);  // Number of repetitions
    rtbf::getReqScalar(R, "nxmits", &nxmits);  // Number of unique acquisitions
    rtbf::getReqScalar(R, "nelems", &nelems);  // Number of total elements
    rtbf::getReqScalar(R, "nchans", &nchans);  // Number of channels per acq.
    rtbf::getReqScalar(R, "nrows", &nrows);    // Number of output image rows
    rtbf::getReqScalar(R, "ncols", &ncols);    // Number of output image columns
    rtbf::getReqScalar(R, "outspwl", &outspwl);  // Samples per wavelength

    // Get optional scalars
    int nframes = 1;
    rtbf::getOptScalar(R, "nframes", &nframes);  // Number of frames

    // Get required delay and apodization tables with error checking
    float *delTx, *apoTx, *delRx, *apoRx;
    rtbf::getDelayApodTables(R, "delTx", &delTx, nrows, ncols, nxmits);
    rtbf::getDelayApodTables(R, "apoTx", &apoTx, nrows, ncols, nxmits);
    rtbf::getDelayApodTables(R, "delRx", &delRx, nrows, ncols, nchans);
    rtbf::getDelayApodTables(R, "apoRx", &apoRx, nrows, ncols, nchans);

    // Get the per-transmit channel mapping
    int *chMap;
    rtbf::getChannelMapping(R, "chanmap", &chMap, nchans, nxmits, nelems);

    // Get the VSXSampleMode
    float spcy;
    rtbf::getVSXSampleMode(R, "sampleMode", &vsxMode, &spcy);

    // Get the synthesis mode
    rtbf::SynthesisMode synMode;
    rtbf::getSynthesisMode(R, "synthesisMode", &synMode);

    // Initialize cudaStream
    CCE(cudaSetDevice(gpuID));
    CCE(cudaStreamCreate(&stream));

    // Initialize GPU objects
    int vv = 2;  // verbosity
    D.initialize(nsamps, nxmits, nelems, nchans, nframes, vsxMode, npulses,
                 chMap, gpuID, stream, vv);
    F.initialize(&D, nrows, ncols, outspwl, delTx, delRx, apoTx, apoRx, spcy,
                 synMode, stream, vv);
    S.initialize(&F, 1, stream, vv);
    B.initialize(&S, stream, vv);

    // Mark as initialized
    initialized = true;
  } else {  // If already initialized, real-time loop

    // Get reconstruction parameters stored in MATLAB structure R
    VSXType *h_raw = (VSXType *)mxGetData(prhs[0]);
    int pitchInSamps = mxGetM(prhs[0]);

    // Format the raw data using the transducer DataFormatter object
    D.formatRawVSXData(h_raw, pitchInSamps);
    F.focus();
    S.sumChannels();
    B.getEnvelopeLogCompress();

    // Copy output to a MATLAB array
    // Outputs ChannelSum, Focus and DataFormatter results if nlhs > 1
    if (nlhs > 0) rtbf::copyToMATLABArrayAsync(&B, &(plhs[0]), stream);
    if (nlhs > 1) rtbf::copyToMATLABArrayAsync(&S, &(plhs[1]), stream);
    if (nlhs > 2) rtbf::copyToMATLABArrayAsync(&F, &(plhs[2]), stream);
    if (nlhs > 3) rtbf::copyToMATLABArrayAsync(&D, &(plhs[3]), stream);
  }
}

// Reset function
void reset() {
  if (initialized) {
    B.reset();
    S.reset();
    F.reset();
    D.reset();
    CCE(cudaStreamDestroy(stream));
    initialized = false;
  }
}

// On MATLAB clear or exit, call reset()
void atExit() { reset(); }

// Dummy function
int main() { return 0; }