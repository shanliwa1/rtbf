/**
 @file vsxBF/utils/VSXDataFormatter.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-13

Copyright 2019 Dongwoon Hyun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "VSXDataFormatter.cuh"

namespace rtbf {
namespace kernels = VSXDataFormatterKernels;

template <typename T>
VSXDataFormatter<T>::VSXDataFormatter() {
  this->resetDataProcessor();
  reset();
}
template <typename T>
VSXDataFormatter<T>::~VSXDataFormatter() {
  reset();
}
template <typename T>
void VSXDataFormatter<T>::reset() {
  if (this->isInit) this->printMsg("Clearing object.");
  CCE(cudaSetDevice(this->devID));

  // VSXDataFormatter members
  nsamps = 0;
  nxmits = 0;
  nchans = 0;
  nacqch = 0;
  npulses = 0;
  nframes = 0;
  mode = VSXSampleMode::HILBERT;
  in.reset();
  chanmap.reset();
  H.reset();

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "VSXDataFormatter");
}

// Initialization function
template <typename T>
void VSXDataFormatter<T>::initialize(int numSamples, int numTransmits,
                                     int numChannels, int numAcqChannels,
                                     int numFrames, VSXSampleMode sampleMode,
                                     int numPulsesToSum, int *h_channelMapping,
                                     int deviceID, cudaStream_t cudaStream,
                                     int verbosity) {
  // If previously initialized, reset
  reset();

  // Populate parameters
  nsamps = numSamples;
  nxmits = numTransmits;
  nchans = numChannels;
  nacqch = numAcqChannels;
  npulses = numPulsesToSum;
  nframes = numFrames;
  mode = sampleMode;
  this->setDeviceID(deviceID);
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  switch (mode) {
    case VSXSampleMode::BS50BW:
      nsamps /= 2;  // Every two samples is the I and Q of one sample
      this->printMsg("BS50BW", 2);
      break;
    case VSXSampleMode::BS100BW:
      nsamps /= 2;  // Every two samples is the I and Q of one sample
      this->printMsg("BS100BW", 2);
      break;
    case VSXSampleMode::NS200BW:
      nsamps /= 2;  // Every two samples is the I and Q of one sample
      this->printMsg("NS200BW", 2);
      break;
    default:
      this->printMsg("HILBERT", 2);
      break;
  }

  // Initialize data arrays on current GPU
  CCE(cudaSetDevice(this->devID));

  // Initialize input DataArray
  in.initialize(numSamples * npulses * nxmits * nframes, nacqch, 1, 1, deviceID,
                "VSXDataFormatter (input)", this->verb);

  // Initialize output DataArray (use output nsamps)
  this->out.initialize(nsamps, nxmits, nchans, nframes, this->devID,
                       "VSXDataFormatter (output)", this->verb);

  // Store the channel mapping as an array on the GPU if it is used
  if (h_channelMapping != nullptr) {
    chanmap.initialize(nacqch * nxmits, 1, 1, 1, this->devID,
                       "VSXDataFormatter (channel map)", this->verb);
    chanmap.copyToGPU(h_channelMapping);
    this->printMsg("Using custom channel mapping.", this->verb);
  }

  // If the VSXSampleMode is HILBERT, also initialize HilbertTransform
  if (mode == VSXSampleMode::HILBERT) {
    H.initialize(this, this->stream, this->verb);
  }

  // Get strides of data based on the data ordering (hard coded for now)
  VSXDataOrder dataOrder = VSXDataOrder::PXF;
  getPitches(&ppitch, &xpitch, &fpitch, dataOrder);

  this->isInit = true;
}

template <typename T>
void VSXDataFormatter<T>::getPitches(int *ppitch, int *xpitch, int *fpitch,
                                     VSXDataOrder order) {
  int ns = nsamps;   // Number of samples
  int np = npulses;  // Number of pulses to sum
  int nx = nxmits;   // Number of transmission locations
  int nf = nframes;  // Number of Doppler frames, i.e., ensemble length
  // Compute strides between consecutive pulses, transmits, and Doppler frames
  if (order == VSXDataOrder::PXF) {
    *ppitch = ns;
    *xpitch = ns * np;
    *fpitch = ns * np * nx;
  } else if (order == VSXDataOrder::PFX) {
    *ppitch = ns;
    *fpitch = ns * np;
    *xpitch = ns * np * nf;
  } else if (order == VSXDataOrder::XPF) {
    *xpitch = ns;
    *ppitch = ns * nx;
    *fpitch = ns * nx * np;
  } else if (order == VSXDataOrder::FPX) {
    *fpitch = ns;
    *ppitch = ns * nf;
    *xpitch = ns * nf * np;
  } else if (order == VSXDataOrder::XFP) {
    *xpitch = ns;
    *fpitch = ns * nx;
    *ppitch = ns * nx * nf;
  } else if (order == VSXDataOrder::FXP) {
    *fpitch = ns;
    *xpitch = ns * nf;
    *ppitch = ns * nf * nx;
  }
}

// This function copies the data from the host to the GPU and selects
// the proper CUDA kernel to execute
template <typename T>
void VSXDataFormatter<T>::formatRawVSXData(short *h_raw, int pitchInSamps) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }

  // Copy data to the input GPU array
  in.copyToGPUAsync(h_raw, this->stream, sizeof(short) * pitchInSamps);

  // Create computation grid
  dim3 B(256, 1, 1);
  dim3 G((nsamps - 1) / B.x + 1, (nxmits - 1) / B.y + 1,
         (nacqch - 1) / B.z + 1);

  // If a channel map is given, use it.
  int *cmap = chanmap.isInitialized() ? chanmap.getDataPtr() : nullptr;

  if (mode == VSXSampleMode::HILBERT) {
    // If treating data as modulated RF
    short *idata = in.getDataPtr();     // Input data pointer
    T *odata = this->out.getDataPtr();  // Output data pointer
    int apitch = in.getNp();            // Input data pitch
    int opitch = this->out.getNp();     // Output data pitch
    kernels::formatRF<<<G, B, 0, this->stream>>>(
        nsamps, npulses, nxmits, nframes, nacqch, nchans, ppitch, xpitch,
        fpitch, idata, apitch, odata, opitch, cmap);
    H.applyHilbertTransform();
  } else {
    // If treating data as baseband IQ
    short2 *idata = (short2 *)in.getDataPtr();  // Input data pointer
    T *odata = this->out.getDataPtr();          // Output data pointer
    int apitch = in.getNp() / 2;                // Input data pitch
    int opitch = this->out.getNp();             // Output data pitch
    if (mode == VSXSampleMode::BS50BW) {        // 1 sample per cycle
      kernels::formatIQ<T, 1><<<G, B, 0, this->stream>>>(
          nsamps, npulses, nxmits, nframes, nacqch, nchans, ppitch, xpitch,
          fpitch, idata, apitch, odata, opitch, cmap);
    } else if (mode == VSXSampleMode::BS100BW) {  // 2 samples per cycle
      kernels::formatIQ<T, 2><<<G, B, 0, this->stream>>>(
          nsamps, npulses, nxmits, nframes, nacqch, nchans, ppitch, xpitch,
          fpitch, idata, apitch, odata, opitch, cmap);
    } else if (mode == VSXSampleMode::NS200BW) {  // 4 samples per cycle
      kernels::formatIQ<T, 4><<<G, B, 0, this->stream>>>(
          nsamps, npulses, nxmits, nframes, nacqch, nchans, ppitch, xpitch,
          fpitch, idata, apitch, odata, opitch, cmap);
    }
  }
}

// Kernel implementations
// Use templates to create separate kernels at compile time
template <typename T>
__global__ void kernels::formatRF(int nsamps, int npulses, int nxmits,
                                  int nframes, int nacqch, int nchans,
                                  int ppitch, int xpitch, int fpitch,
                                  short *idata, int apitch, T *odata,
                                  int opitch, int *cmap) {
  // Determine information about the current thread
  int samp = blockIdx.x * blockDim.x + threadIdx.x;
  int xmit = blockIdx.y * blockDim.y + threadIdx.y;
  int acqc = blockIdx.z * blockDim.z + threadIdx.z;  // Acquisition channel

  // Only execute kernel if valid
  if (samp < nsamps && xmit < nxmits && acqc < nacqch) {
    // Output channel index; destination of current data
    int chan = cmap == nullptr ? acqc : cmap[acqc + nacqch * xmit];
    // Advance input and output data pointers
    idata += samp + xmit * xpitch + acqc * apitch;
    odata += samp + opitch * (xmit + nxmits * chan);
    // Loop through Doppler frames
    for (int f = 0; f < nframes; f++) {
      float sum = 0.f;
      for (int p = 0; p < npulses; p++) {
        sum += idata[p * ppitch + f * fpitch];
      }
      // Cast sum to correct datatype and store result in odata
      saveData2(odata, f * opitch * nxmits * nchans, sum);
    }
  }
}

template <typename T, int spc>
__global__ void kernels::formatIQ(int nsamps, int npulses, int nxmits,
                                  int nframes, int nacqch, int nchans,
                                  int ppitch, int xpitch, int fpitch,
                                  short2 *idata, int apitch, T *odata,
                                  int opitch, int *cmap) {
  // Determine information about the current thread
  int samp = blockIdx.x * blockDim.x + threadIdx.x;
  int xmit = blockIdx.y * blockDim.y + threadIdx.y;
  int acqc = blockIdx.z * blockDim.z + threadIdx.z;  // Acquisition channel

  // Only execute kernel if valid
  if (samp < nsamps / 2 + 64 && xmit < nxmits && acqc < nacqch) {
    // Output channel index; destination of current data
    int chan = cmap == nullptr ? acqc : cmap[acqc + nacqch * xmit];
    // Advance input and output data pointers
    idata += samp + xmit * xpitch + acqc * apitch;
    odata += samp + opitch * (xmit + nxmits * chan);
    // Loop through the number of summed pulses (i.e., for harmonic imaging)
    float2 sum = make_float2(0.f, 0.f);
    for (int f = 0; f < nframes; f++) {
      for (int p = 0; p < npulses; p++) {
        float2 data;
        data.x = (float)idata[p * ppitch + f * fpitch].x;
        data.y = (float)idata[p * ppitch + f * fpitch].y;
        // Align the quadrature sample using linear interpolation, i.e.
        //   (I0, Q1, I2, Q3, ...) --> (I0, Q0, I2, Q2, ...)
        // Do so by interpolating Q with previous Q
        float qprev = samp > 0 ? idata[samp - 1].y : 0.f;
        // If 4 samples per cycle, every 3rd and 4th sample are negated.
        if (spc == 4) {
          int sign = (1 - 2 * (samp & 0x1));  // Use bitwise arithmetic
          data *= sign;
          qprev *= -sign;
        }
        // Apply linear interpolation with previous Q sample
        float frac = 1.f * spc / 8.f;
        data.y = -1.f * (data.y * (1 - frac) + qprev * frac);
        // Add the properly aligned samples to the running sum
        sum += data;
      }
      // Cast sum to correct datatype and store result in odata
      saveData2(odata, f * opitch * nxmits * nchans, sum);
    }
  }
}

// Explicitly instantiate template for short2, float2 outputs
template class VSXDataFormatter<short2>;
template class VSXDataFormatter<float2>;
}
