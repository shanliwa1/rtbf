cmake_minimum_required(VERSION 3.13)
project(vsxBF_utils LANGUAGES CUDA CXX)

# The FindMatlab.cmake module is broken for CUDA related tasks.
# Use the modified one bundled with vsxBF instead
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/CMakeModules/" ${CMAKE_MODULE_PATH})
find_package(Matlab COMPONENTS MX_LIBRARY REQUIRED)

include_directories(
		${CMAKE_SOURCE_DIR}/gpuBF
		${CMAKE_SOURCE_DIR}/vsxBF/formatters/
        ${CUDA_SAMPLES_INCLUDE_DIRECTORIES}
        ${Matlab_INCLUDE_DIRS})

# Make class objects using function defined in the parent CMakeLists.txt
MAKE_CLASS_OBJECT(VSXDataFormatter)
MAKE_CLASS_OBJECT(vsxBF_utils)

# add_library(vsxBF_utils STATIC
#     VSXDataFormatter.cu
#     VSXDataFormatter.cuh
#     vsxBF_utils.cu
#     vsxBF_utils.cuh
# )
# set_target_properties(vsxBF_utils PROPERTIES
# 		CUDA_SEPARABLE_COMPILATION ON
#         POSITION_INDEPENDENT_CODE ON
# )

