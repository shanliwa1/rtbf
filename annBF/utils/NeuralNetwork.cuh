/**
 @file annBF/utils/NeuralNetwork.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-13

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef NEURALNETWORK_CUH_
#define NEURALNETWORK_CUH_

#include <NvOnnxParser.h>
#include <NvUffParser.h>
#include <NvUtils.h>
#include "DataProcessor.cuh"

namespace rtbf {

class Logger : public nvinfer1::ILogger {
  void log(nvinfer1::ILogger::Severity severity, const char *msg) override {
    // suppress info-level messages
    if (severity != Severity::kINFO) std::cout << msg << std::endl;
  }
};

/**	@brief Class to use a neural network as a DataProcessor

*/
template <typename T_in, typename T_out>
class NeuralNetwork : public DataProcessor<T_out> {
 private:
  // Input DataArray
  DataArray<T_in> *in;

  // Neural network objects
  char net_filename[1024];
  char input_node[256];
  char output_node[256];
  Logger gLogger;
  nvinfer1::ICudaEngine *engine;
  nvinfer1::IExecutionContext *context;
  void *buffers[2];
  int batchSize = 1;
  int iBufIdx, oBufIdx;  // Buffer indices to bind

  enum class FileType { UFF = 0, ONNX = 1 };
  FileType net_filetype;

 public:
  NeuralNetwork();
  virtual ~NeuralNetwork();

  // Initialization steps
  void initialize(DataArray<T_in> *input, const char *networkFileName,
                  const char *inputNodeName = "input",
                  const char *outputNodeName = "output",
                  cudaStream_t cudaStream = 0, int verbosity = 1);
  void initialize(DataProcessor<T_in> *input, const char *networkFileName,
                  const char *inputNodeName = "input",
                  const char *outputNodeName = "output",
                  cudaStream_t cudaStream = 0, int verbosity = 1);
  /// @brief Set a new input DataArray
  void setInputDataArray(DataArray<T_in> *input);
  /// @brief Set a new input DataArray from a DataProcessor
  void setInputDataProcessor(DataProcessor<T_in> *input) {
    setInputDataArray(input->getOutputDataArray());
  }
  void reset();

  // Function to execute network
  void execute();
};
}  // namespace rtbf

#endif /* NEURALNETWORK_CUH_ */
