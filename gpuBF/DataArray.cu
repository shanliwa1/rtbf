/**
 @file gpuBF/DataArray.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-04

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "DataArray.cuh"

namespace rtbf {

// Don't use the constructor or destructor themselves because the
// DataArrays will often be declared statically. Instead, have the
// user use the initialize and reset functions.
template <typename T>
DataArray<T>::DataArray() {
  isInit = false;  // Just in case...
  reset();
}
template <typename T>
DataArray<T>::~DataArray() {
  reset();
}

template <typename T>
void DataArray<T>::initialize(int nelemx, int nelemy, int nchans, int nframes,
                              int deviceID, const char *dataLabel,
                              int verbosity, bool pitched) {
  // Parse inputs
  dims.x = nelemx;
  dims.y = nelemy;
  dims.c = nchans;
  dims.f = nframes;
  setDeviceID(deviceID);
  strcpy(label, dataLabel);

  // Validate the input dimensions
  validateDimensions();

  if (pitched) {
    // Allocate a pitched array
    CCE(cudaSetDevice(devID));
    CCE(cudaMallocPitch((void **)&d_ptr, &pitchInBytes, dims.x * sizeof(T),
                        dims.y * dims.c * dims.f));
  } else {
    // Allocate linear memory
    CCE(cudaSetDevice(devID));
    CCE(cudaMalloc((void **)&d_ptr,
                   sizeof(T) * dims.x * dims.y * dims.c * dims.f));
    pitchInBytes = sizeof(T) * dims.x;
  }

  // Get the pitch in elements in the first dimension
  dims.p = int(pitchInBytes / sizeof(T));
  // Initialize all values to zero
  CCE(cudaMemset(d_ptr, 0, pitchInBytes * dims.y * dims.c * dims.f));

  if (verbosity > 1) printDimensions();

  // Mark as initialized
  isInit = true;
}

template <typename T>
void DataArray<T>::initialize(DataDim dimensions, int deviceID,
                              const char *dataLabel, int verbosity,
                              bool pitched) {
  initialize(dimensions.x, dimensions.y, dimensions.c, dimensions.f, deviceID,
             dataLabel, verbosity, pitched);
}

template <typename T>
void DataArray<T>::reset() {
  if (isInit) {
    CCE(cudaSetDevice(devID));
    CCE(cudaFree(d_ptr));
  }
  d_ptr = nullptr;
  devID = 0;
  strcpy(label, "");
  isInit = false;
  dims.x = 0;
  dims.p = 0;
  dims.y = 0;
  dims.c = 0;
  dims.f = 0;
  typeSize = sizeof(T);
  pitchInBytes = 0;
}

// Utility functions
template <typename T>
void DataArray<T>::validateDimensions() {
  if (dims.x <= 0 || dims.y <= 0 || dims.c <= 0 || dims.f <= 0) {
    fprintf(stderr,
            "[gpu%d %s] %s: Invalid input size parameters (%d, %d, %d, %d).\n",
            devID, devName, label, dims.x, dims.y, dims.c, dims.f);
  }
}
template <typename T>
void DataArray<T>::printDimensions() {
  printf("[gpu%d %s] %s size: %d (%d) x %d x %d (%d frames)\n", devID, devName,
         label, dims.x, dims.p, dims.y, dims.c, dims.f);
}

// Functions to copy data to and from CPU
template <typename T>
void DataArray<T>::copyToGPU(T *h_ptr, size_t h_pitchInBytes) {
  // If omitted, assume the pitch matches the first data dimension
  if (h_pitchInBytes == 0) h_pitchInBytes = sizeof(T) * dims.x;
  CCE(cudaSetDevice(devID));
  CCE(cudaMemcpy2D(d_ptr, pitchInBytes, h_ptr, h_pitchInBytes,
                   sizeof(T) * dims.x, dims.y * dims.c * dims.f, HtoD));
}
template <typename T>
void DataArray<T>::copyFromGPU(T *h_ptr, size_t h_pitchInBytes) {
  // If omitted, assume the pitch matches the first data dimension
  if (h_pitchInBytes == 0) h_pitchInBytes = sizeof(T) * dims.x;
  CCE(cudaSetDevice(devID));
  CCE(cudaMemcpy2D(h_ptr, h_pitchInBytes, d_ptr, pitchInBytes,
                   sizeof(T) * dims.x, dims.y * dims.c * dims.f, DtoH));
}
template <typename T>
void DataArray<T>::copyToGPUAsync(T *h_ptr, cudaStream_t stream,
                                  size_t h_pitchInBytes) {
  // If omitted, assume the pitch matches the first data dimension
  if (h_pitchInBytes == 0) h_pitchInBytes = sizeof(T) * dims.x;
  CCE(cudaSetDevice(devID));
  CCE(cudaMemcpy2DAsync(d_ptr, pitchInBytes, h_ptr, h_pitchInBytes,
                        sizeof(T) * dims.x, dims.y * dims.c * dims.f, HtoD,
                        stream));
}
template <typename T>
void DataArray<T>::copyFromGPUAsync(T *h_ptr, cudaStream_t stream,
                                    size_t h_pitchInBytes) {
  // If omitted, assume the pitch matches the first data dimension
  if (h_pitchInBytes == 0) h_pitchInBytes = sizeof(T) * dims.x;
  CCE(cudaSetDevice(devID));
  CCE(cudaMemcpy2DAsync(h_ptr, h_pitchInBytes, d_ptr, pitchInBytes,
                        sizeof(T) * dims.x, dims.y * dims.c * dims.f, DtoH,
                        stream));
}

template class DataArray<float2>;
template class DataArray<short2>;
template class DataArray<int2>;
template class DataArray<float>;
template class DataArray<short>;
template class DataArray<int>;

}  // namespace rtbf
