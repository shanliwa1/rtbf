/**
 @file gpuBF/Concatenate.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2020-01-21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Concatenate.cuh"

namespace rtbf {
namespace kernels = ConcatenateKernels;
template <typename T_in, typename T_out>
Concatenate<T_in, T_out>::Concatenate() {
  this->resetDataProcessor();
  reset();
}
template <typename T_in, typename T_out>
Concatenate<T_in, T_out>::~Concatenate() {
  reset();
}

template <typename T_in, typename T_out>
void Concatenate<T_in, T_out>::reset() {
  if (this->isInit) this->printMsg("Clearing object.");

  // Concatenate class members
  for (int n = 0; n < Concatenate<T_in, T_out>::MAXINPUTS; n++) {
    in[n] = nullptr;
  }

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "Concatenate");
}

template <typename T_in, typename T_out>
void Concatenate<T_in, T_out>::initialize(int numInputs,
                                          DataArray<T_in> *inputs[],
                                          cudaStream_t cudaStream,
                                          int verbosity) {
  // If previously initialized, reset
  reset();

  // Check if number of inputs is valid
  ninputs = numInputs;
  if (ninputs < 2 || ninputs > Concatenate<T_in, T_out>::MAXINPUTS) {
    this->printErr("Invalid number of inputs.", __FILE__, __LINE__);
  }

  // Read inputs
  DataDim d0 = inputs[0]->getDataDim();
  in[0] = inputs[0];
  int nchans = d0.c;
  for (int n = 1; n < ninputs; n++) {
    DataDim dn = inputs[n]->getDataDim();
    // Validate dimensions
    if (dn.x != d0.x || dn.y != d0.y || dn.f != d0.f) {
      this->printErr("The dimensions of the DataArrays do not match.", __FILE__,
                     __LINE__);
    }
    in[n] = inputs[n];
    nchans += dn.c;
  }

  // Device information
  this->setDeviceID(inputs[0]->getDeviceID());
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  // Make a new DataArray for the output
  this->out.initialize(d0.x, d0.y, nchans, d0.f, this->devID,
                       "Concatenated DataArray", this->verb);

  // Initialize arrays
  this->isInit = true;
}

template <typename T_in, typename T_out>
void Concatenate<T_in, T_out>::initialize(int numInputs,
                                          DataProcessor<T_in> *inputs[],
                                          cudaStream_t cudaStream,
                                          int verbosity) {
  // Create temporary array of pointers to DataArrays
  DataArray<T_in> *tmp[numInputs];
  for (int n = 0; n < numInputs; n++) {
    tmp[n] = new DataArray<T_in>;
    tmp[n] = inputs[n]->getOutputDataArray();
  }
  // Pass through to initializer
  initialize(numInputs, tmp, cudaStream, verbosity);
}

template <typename T_in, typename T_out>
void Concatenate<T_in, T_out>::concatenate() {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }

  // Get output data pointer
  T_out *d_out = this->out.getDataPtr();
  DataDim odims = this->out.getDataDim();

  // Iterate over all DataArrays
  for (int n = 0; n < ninputs; n++) {
    // Get the input data pointer and dimensions
    T_in *d_in = in[n]->getDataPtr();
    DataDim idims = in[n]->getDataDim();

    // Set kernel parameters
    dim3 B(256, 1, 1);
    dim3 G((odims.x - 1) / B.x + 1, (odims.y - 1) / B.y + 1,
           (idims.c - 1) / B.z + 1);

    // Execute
    kernels::copyChannels<<<G, B, 0, this->stream>>>(d_in, idims, d_out, odims);

    // Advance output pointer by the number of input channels
    d_out += odims.p * odims.y * idims.c;
  }
}

///////////////////////////////////////////////////////////////////////////
// Kernels
///////////////////////////////////////////////////////////////////////////
template <typename T_in, typename T_out>
__global__ void kernels::copyChannels(T_in *idata, DataDim idims, T_out *odata,
                                      DataDim odims) {
  int x = threadIdx.x + blockIdx.x * blockDim.x;
  int y = threadIdx.y + blockIdx.y * blockDim.y;
  int c = threadIdx.z + blockIdx.z * blockDim.z;
  if (x < odims.x && y < odims.y && c < idims.c) {
    // Advance data pointers to current index
    idata += x + idims.p * (y + idims.y * c);
    odata += x + odims.p * (y + odims.y * c);
    // Loop through frames
    int ifsize = idims.p * idims.y * idims.c;
    int ofsize = odims.p * odims.y * odims.c;
    for (int f = 0; f < odims.f; f++) {
      odata[f * ofsize] = (T_out)idata[f * ifsize];
    }
  }
}

// Explicit template specialization instantiation
template class Concatenate<short2, short2>;
template class Concatenate<float2, float2>;
template class Concatenate<short, short>;
template class Concatenate<float, float>;

}  // namespace rtbf
