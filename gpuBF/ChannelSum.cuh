/**
 @file gpuBF/ChannelSum.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-04

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CHANNELSUM_CUH_
#define CHANNELSUM_CUH_

#include "DataProcessor.cuh"

namespace rtbf {

/**	@brief Class to sum channel signals.

This class sums channel data. Optionally, this class can be used partially sum
the array, specified by the number of output channels. This can be useful for
tasks such as spatial compounding.
*/
template <typename T_in, typename T_out>
class ChannelSum : public DataProcessor<T_out> {
 private:
  DataArray<T_in> *in;  ///< Input DataArray
  int nchans_output;    ///< Number of channels to output (1 by default, > 1 for
                        /// subaperture beamforming)

 public:
  ChannelSum();
  virtual ~ChannelSum();

  /// @brief Initialize ChannelSum object with a DataArray input
  void initialize(DataArray<T_in> *input, int nOutputChannels = 1,
                  cudaStream_t cudaStream = 0, int verbosity = 1);
  /// @brief Initialize ChannelSum object with a DataProcessor input
  void initialize(DataProcessor<T_in> *input, int nOutputChannels = 1,
                  cudaStream_t cudaStream = 0, int verbosity = 1);
  /// @brief Reset object
  void reset();

  /// @brief Sum channels together
  void sumChannels(bool normalize = false);
  /// @brief Set a new input DataArray
  void setInputDataArray(DataArray<T_in> *input);
  /// @brief Set a new input DataArray from a DataProcessor
  void setInputDataProcessor(DataProcessor<T_in> *input) {
    setInputDataArray(input->getOutputDataArray());
  }
};

namespace ChannelSumKernels {
template <typename T_in, typename T_out>
__global__ void sumChannels(T_in *idata, DataDim idims, T_out *odata,
                            DataDim odims, int ds);
template <typename T_in, typename T_out>
__global__ void avgChannels(T_in *idata, DataDim idims, T_out *odata,
                            DataDim odims, int ds);
}  // namespace ChannelSumKernels
}  // namespace rtbf

#endif /* CHANNELSUM_CUH_ */
