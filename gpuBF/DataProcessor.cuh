/**
 @file gpuBF/DataProcessor.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-03

Copyright 2019 Dongwoon Hyun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef DATAPROCESSOR_CUH_
#define DATAPROCESSOR_CUH_

#include "DataArray.cuh"
#include "gpuBF.cuh"

namespace rtbf {

/** @brief Abstract base class for processing data on the GPU.

All DataProcessor objects have a general structure that is defined in gpuBF.cuh.
Each DataProcessor object (or child object) is a module that can be
daisy-chained with other such modules. Ideally, one DataProcessor object can be
passed as the input to another DataProcessor object, and the initializer code of
that second object should know how to parse the output of the first. For
example, the HilbertTransform object can be fed into the Focus object, which can
in turn be fed into the Bmode object, and each object should be able to invoke
the various get* functions of the previous object to determine how to allocate
its own arrays.

This class is an abstract base class, meaning that a DataProcessor object cannot
be instantiated on its own. To be usable, child classes must explicitly define
the virtual functions defined in the DataProcessor base class.

*/
template <typename T_out>
class DataProcessor {
 protected:
  // Device information and CUDA objects
  int devID;  ///<Index of the GPU that the device arrays of this object will
              /// reside on
  char devName[32];     ///<Name of GPU that the device array resides on
  cudaStream_t stream;  ///<The CUDA stream on which all real-time operations
                        /// will execute
  bool isInit;  ///< Boolean to indicate whether arrays have been allocated an
                /// object is ready for use
  int verb;     ///<Level of verbosity (0 = no output)
  DataArray<T_out> out;  ///< Output DataArray
  char moniker[32];      ///< Name of DataProcessor

  /// @brief Sets the GPU device ID for this object
  void setDeviceID(int deviceID) {
    devID = deviceID;
    setDeviceName();
  }
  /// @brief Sets the GPU name for this object
  void setDeviceName() {
    cudaDeviceProp props;
    CCE(cudaGetDeviceProperties(&props, this->devID));
    strcpy(this->devName, props.name);
  }
  /// @brief Reset DataProcessor objects
  void resetDataProcessor() {
    devID = 0;
    stream = 0;
    isInit = false;
    verb = 0;
    out.reset();
  }

 public:
  /// @brief Returns the GPU index for this object
  int getDeviceID() { return devID; }
  /// @brief Returns the GPU name for this object
  char *getDeviceName() { return devName; }
  /// @brief Returns the CUDA stream for this object
  cudaStream_t getCudaStream() { return stream; }
  /// @brief Returns whether the object has been initialized
  bool isInitialized() { return isInit; }
  /// @brief Returns the level of verbosity
  int getVerbosity() { return verb; }
  /// @brief Returns the output DataArray
  virtual DataArray<T_out> *getOutputDataArray() { return &out; }

  // Require a function to free dynamically allocated memory
  /// @brief A function that frees all dynamically allocated arrays owned by the
  /// object. Must be redefined by each child class
  virtual void reset() = 0;

  /// @brief Utility function to print messages to stdout
  void printMsg(const char *msg, int verbLevel = 1) {
    if (verb >= verbLevel) {
      printf("[gpu%d %s] %s: %s\n", this->devID, this->devName, moniker, msg);
    }
  }
  /// @brief Utility function to print messages to stderr
  void printErr(const char *msg, const char *file, int line) {
    fprintf(stderr, "%s(%i) : GPU %d (%s) %s: %s\n", file, line, this->devID,
            this->devName, moniker, msg);
  }
};
template class DataProcessor<float2>;
template class DataProcessor<short2>;
template class DataProcessor<int2>;
template class DataProcessor<float>;
template class DataProcessor<short>;
template class DataProcessor<int>;

}  // namespace rtbf

#endif /* DATAPROCESSOR_CUH_ */
