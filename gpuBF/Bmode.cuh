/**
 @file gpuBF/Bmode.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-11

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef BMODE_CUH_
#define BMODE_CUH_

#include "DataProcessor.cuh"

namespace rtbf {

/**	@brief Class to make B-mode images from delay-and-summed data.

This class accepts delay-and-summed data (i.e., from ChannelSum), and detects
the envelope and applies any log compression as desired. If the input number of
channels is greater than 1, incoherent compounding is automatically performed in
that dimension.
*/
template <typename T_in, typename T_out>
class Bmode : public DataProcessor<T_out> {
 private:
  DataArray<T_in> *in;  ///< Input DataArray

 public:
  Bmode();
  virtual ~Bmode();

  /// @brief Initialize Bmode object with a DataArray input
  void initialize(DataArray<T_in> *input, cudaStream_t cudaStream = 0,
                  int verbosity = 1);
  /// @brief Initialize Bmode object with a DataProcessor input
  void initialize(DataProcessor<T_in> *input, cudaStream_t cudaStream = 0,
                  int verbosity = 1);
  /// @brief Reset object
  void reset();

  /// @brief Detect magnitude of complex data
  void getEnvelope();
  /// @brief Detect magnitude of complex data and apply logarithmic compression
  void getEnvelopeLogCompress();
  /// @brief Get power of an ensemble of complex data
  // void
  /// @brief Set a new input DataArray
  void setInputDataArray(DataArray<T_in> *input);
  /// @brief Set a new input DataArray from a DataProcessor
  void setInputDataProcessor(DataProcessor<T_in> *input) {
    setInputDataArray(input->getOutputDataArray());
  }
};

namespace BmodeKernels {
template <typename T_in, typename T_out>
__global__ void envDetect(T_in *idata, DataDim idims, T_out *odata,
                          DataDim odims);

template <typename T_in, typename T_out>
__global__ void envDetectLogComp(T_in *idata, DataDim idims, T_out *odata,
                                 DataDim odims);
}  // namespace BmodeKernels
}  // namespace rtbf

#endif /* BMODE_CUH_ */
