/**
 @file gpuBF/Focus.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-03

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef FOCUS_CUH_
#define FOCUS_CUH_

#include "DataProcessor.cuh"

namespace rtbf {

/** @brief Class to apply focusing delays to data. The output is always
demodulated focused channel data.

This class applies focusing delays and apodization profiles to the inputted
data. The data must be complex, but can be either radiofrequency or baseband.
The user provides delay and apodization profiles for the data. No aperture
synthesis is performed.
*/
template <typename T_in, typename T_out>
class Focus : public DataProcessor<T_out> {
 protected:
  // CUDA objects
  cudaTextureObject_t tex;
  cudaTextureDesc texDesc;
  cudaResourceDesc resDesc;

  DataArray<T_in> *raw;  ///< Pointer to input unfocused DataArray
  DataArray<float> del;  ///< Delay table
  DataArray<float> apo;  ///< Apodization table
  float outwlps;         ///< Output wavelengths per sample
  float spc;             ///< Input samples per cycle for demodulated data

  // Internal functions
  void initTextureObject(T_in *d_ptr);

 public:
  // Constructor and destructor
  Focus();
  virtual ~Focus();

  // Initialization steps
  /** @brief Function to initialize the object with DataArray input.
  @param input Pointer to input DataArray.
  @param nOutputRows Number of rows in output array.
  @param nOutputCols Number of columns in output array.
  @param outputSampsPerWL Number of samples per wavelength in output image.
  @param h_del Pointer to host array containing focusing delays.
  @param h_apo Pointer to host array containing apodization (optional).
  @param samplesPerCycle Demodulation samples per cycle (baseband mode).
  @param cudaStream cudaStream to use. Defaults to 0.
  @param verbosity Level of verbosity. Defaults to 1.
  */
  void initialize(DataArray<T_in> *input, int nOutputRows, int nOutputCols,
                  float outputSampsPerWL, float *h_del, float *h_apo = nullptr,
                  float samplesPerCycle = 0.f, cudaStream_t cudaStream = 0,
                  int verbosity = 1);
  /** @brief Function to initialize the object with DataProcessor input.
  @param input Pointer to input DataProcessor.
  @param nOutputRows Number of rows in output array.
  @param nOutputCols Number of columns in output array.
  @param outputSampsPerWL Number of samples per wavelength in output image.
  @param h_del Pointer to host array containing focusing delays.
  @param h_apo Pointer to host array containing apodization (optional).
  @param samplesPerCycle Demodulation samples per cycle (baseband mode).
  @param cudaStream cudaStream to use. Defaults to 0.
  @param verbosity Level of verbosity. Defaults to 1.
  */
  void initialize(DataProcessor<T_in> *input, int nOutputRows, int nOutputCols,
                  float outputSampsPerWL, float *h_del, float *h_apo = nullptr,
                  float samplesPerCycle = 0.f, cudaStream_t cudaStream = 0,
                  int verbosity = 1);

  /// @brief Add a global delay to the delay table
  void addGlobalDelay(float globalDelay);
  /// @brief Set a new input DataArray
  void setInputDataArray(DataArray<T_in> *input);
  /// @brief Set a new input DataArray from a DataProcessor
  void setInputDataProcessor(DataProcessor<T_in> *input) {
    setInputDataArray(input->getOutputDataArray());
  }
  /// @brief Free all dynamically allocated memory.
  void reset();
  /// @brief Apply focusing and apodization to data
  void focus();
};

namespace FocusKernels {
/// @cond KERNELS
/**	@addtogroup FocusKernels Focus Kernels
        @{
*/

/** @brief Kernel to focus modulated data in a synthetic aperture configuration
(without apodization).
@param dims DataDim of the output.
@param tex Texture object of the raw data.
@param d_del Delay table.
@param delPitch Pitch of the delay tables (in elements).
@param d_out Device pointer to the output focused data.
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param normFactor Normalization factor for float vs. int texture fetching.
*/
template <typename T_out>
__global__ void focusRF(DataDim dims, cudaTextureObject_t tex, float *d_del,
                        int delPitch, T_out *d_out, float outwlps,
                        float normFactor);
/** @brief Kernel to focus modulated data in a synthetic aperture configuration
with transmit and receive apodization.
@param dims DataDim of the output.
@param tex Texture object of the raw data.
@param d_del Delay table.
@param d_apo Apodization table.
@param delPitch Pitch of all delay and apodization tables (in elements).
@param d_out Device pointer to the output focused data.
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param normFactor Normalization factor for float vs. int texture fetching.
*/
template <typename T_out>
__global__ void focusRFApod(DataDim dims, cudaTextureObject_t tex, float *d_del,
                            float *d_apo, int delPitch, T_out *d_out,
                            float outwlps, float normFactor);
/** @brief Kernel to focus baseband data in a synthetic aperture configuration
(without apodization).
@param dims DataDim of the output.
@param tex Texture object of the raw data.
@param d_del Delay table.
@param delPitch Pitch of the delay tables (in elements).
@param d_out Device pointer to the output focused data.
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param normFactor Normalization factor for float vs. int texture fetching.
@param cyclesPerSample Number of cycles per sample (fc/fs) of <i>input</i> data.
*/
template <typename T_out>
__global__ void focusBB(DataDim dims, cudaTextureObject_t tex, float *d_del,
                        int delPitch, T_out *d_out, float outwlps,
                        float normFactor, float cyclesPerSample);
/** @brief Kernel to focus baseband data in a synthetic aperture configuration
with transmit and receive apodization.
@param dims DataDim of the output.
@param tex Texture object of the raw data.
@param d_del Delay table.
@param d_apo Apodization table.
@param delPitch Pitch of all delay and apodization tables (in elements).
@param d_out Device pointer to the output focused data.
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param normFactor Normalization factor for float vs. int texture fetching.
@param cyclesPerSample Number of cycles per sample (fc/fs) of <i>input</i> data.
*/
template <typename T_out>
__global__ void focusBBApod(DataDim dims, cudaTextureObject_t tex, float *d_del,
                            float *d_apo, int delPitch, T_out *d_out,
                            float outwlps, float normFactor,
                            float cyclesPerSample);

/** @brief Kernel to add a single global delay to a delay table. Useful for when
different imaging configurations have different time zero values.
@param dims DataDim of the delay table.
@param del Delay table.
@param globalDelay Scalar constant to add to delay table.
*/
__global__ void addGlobalDelay(DataDim dims, float *del, float globalDelay);
/** @}*/
/// @endcond

}  // namespace FocusKernels
}  // namespace rtbf

#endif /* FOCUS_CUH_ */
