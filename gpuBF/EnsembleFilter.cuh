/**
 @file gpuBF/HilbertTransform.cuh
 @author Dongwoon Hyun (dhyun)
 @date 2019-07-30

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef ENSEMBLEFILTER_CUH_
#define ENSEMBLEFILTER_CUH_

#include "DataProcessor.cuh"
#define MAXENSEMBLELENGTH 16

namespace rtbf {
/** @brief Class to apply a slow-time ensemble filter for Doppler applications.

This class takes a DataArray of M frames and performs an in-place filtering
using an MxM matrix of filter coefficients. Matrix multiplication is performed
using CUBLAS.
*/

template <typename T>
class EnsembleFilter : public DataProcessor<T> {
 private:
  DataArray<T> *in;       ///< Input DataArray
  DataArray<float> filt;  ///< Filter coefficients
  int M;                  ///< Number of rows in filter matrix
  int N;                  ///< Number of columns in filter matrix

 public:
  EnsembleFilter();
  virtual ~EnsembleFilter();
  /// @brief Initialize EnsembleFilter object with a DataArray input
  void initialize(DataArray<T> *input, int nrows, int ncols,
                  float *h_filterMatrix, cudaStream_t cudaStream = 0,
                  int verbosity = 1);
  /// @brief Initialize EnsembleFilter object with a DataProcessor input
  void initialize(DataProcessor<T> *input, int nrows, int ncols,
                  float *h_filterMatrix, cudaStream_t cudaStream = 0,
                  int verbosity = 1);
  /// @brief Reset object
  void reset();

  /// @brief Apply filter across ensemble
  void filterEnsemble();
};

namespace EnsembleFilterKernels {
template <typename T>
__global__ void filter(T *dat, DataDim datSize, int M, int N, float *filt);
}  // namespace EnsembleFilterKernels
}  // namespace gpuBF

#endif /* ENSEMBLEFILTER_CUH_ */
