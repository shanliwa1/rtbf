/**
 @file gpuBF/HilbertTransform.cuh
 @author Dongwoon Hyun (dhyun)
 @date 2019-07-23

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef HILBERTTRANSFORM_CUH_
#define HILBERTTRANSFORM_CUH_

#include <cufft.h>
#include <typeinfo>
#include "DataProcessor.cuh"

namespace rtbf {
/** @brief Class to apply in-place Hilbert Transform

This class takes a real signal (stored as an interleaved complex array with
zeros for the imaginary component) and computes the Hilbert Transform using
the cuFFT API. The transform is accomplished via a forward FFT, eliminating
the negative frequencies and doubling the positive frequencies, followed by a
final inverse FFT.
*/
template <typename T>
class HilbertTransform : public DataProcessor<T> {
 private:
  DataArray<T> *in;               ///< Input DataArray
  cufftHandle fftplan;            ///< CUFFT plan object
  DataArray<float2> *fft;         ///< Temporary DataArray (if T != float2)
  void initFFTDataArray();        ///< Initialize fft (if necessary)
  void resetFFTDataArray();       ///< Reset fft (if necessary)
  void castToFloat2IfNeeded();    ///< Cast to float2 (if necessary)
  void castFromFloat2IfNeeded();  ///< Cast from float2 (if necessary)

 public:
  HilbertTransform();
  virtual ~HilbertTransform();

  /// @brief Initialize HilbertTransform object with a DataArray input
  void initialize(DataArray<T> *input, cudaStream_t cudaStream = 0,
                  int verbosity = 1);
  /// @brief Initialize HilbertTransform object with a DataProcessor input
  void initialize(DataProcessor<T> *input, cudaStream_t cudaStream = 0,
                  int verbosity = 1);
  /// @brief Reset object
  void reset();

  /// @brief Hilbert transform of raw RF data
  void applyHilbertTransform();
  /// @brief Set a new input DataArray
  void setInputDataArray(DataArray<T> *input);
  /// @brief Set a new input DataArray from a DataProcessor
  void setInputDataProcessor(DataProcessor<T> *input) {
    setInputDataArray(input->getOutputDataArray());
  }
  /// @brief Override the getOutputDataArray function for in-place processing
  DataArray<T> *getOutputDataArray() { return in; }
};

namespace HilbertTransformKernels {
__global__ void makeAnalyticOdd(float2 *data, DataDim dim);
__global__ void makeAnalyticEven(float2 *data, DataDim dim);
template <typename T>
__global__ void castToTypeFloat2(T *in, DataDim idim, float2 *out, int odim_p);
template <typename T>
__global__ void castFromTypeFloat2(float2 *in, DataDim idim, T *out,
                                   int odim_p);
}  // namespace HilbertTransformCallbacks
}  // namespace rtbf

#endif /* HILBERTTRANSFORM_CUH_ */
